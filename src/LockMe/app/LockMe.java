package LockMe.app;

import LockMe.user.UserCredential;
import LockMe.user.UserRegistration;

import java.util.Scanner;
import java.io.FileWriter;
import java.io.PrintWriter;

import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;


public class LockMe {

			private static PrintWriter display, display1;
            private static Scanner input, input1, input2;
            private static String userlogin;
            private static UserCredential userCredential;
            private static UserRegistration userRegistration;
            

            public static void main(String[] args) {

                                    firstload();
                                    homePage();
            }

            static void firstload() {
                File userReg = new File("userLogin.txt");
                File userCred = new File("userCredential.txt");

                try {
                           display = new PrintWriter(new FileWriter(userReg, true));
                            display1 = new PrintWriter(new FileWriter(userCred, true));
                            input = new Scanner(System.in);
                } catch (IOException e) {
                            e.printStackTrace();
                }
                userRegistration = new UserRegistration();
                userCredential = new UserCredential();
    }
            
            static void homePage()  {
                       
                        System.out.println("");
                        System.out.println(" Welcome to LockedMe Registration Page ");
                        System.out.println();
                        
                        regOption();
            }


            public static void regOption()  {
                        System.out.println("1. Registration");
                        System.out.println("2. Login");
           
                        String selection = input.next();
                        switch (selection) {
                        case "1":
                                    registration();
                                    break;
                        case "2":
                                    login();
                                    break;
                        default:
                                    System.out.println("Please select either option 1 or 2");
                                    regOption();
                                    break;
                        }
            }

          
            public static void registration()  {
                        System.out.print("Enter the username");
                        String username = input.next();
                        boolean found = false;
                        try {
                                    input1 = new Scanner(new File("userLogin.txt"));
                        } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                        }
                        while (input1.hasNext() && !found) {
                                    String storedString = input1.next();
                                    String storedUsername[] = storedString.split(":");
                                    if (storedUsername[0].equals(username)) {
                                                System.out.println("User already exists. Please select another user for registration");
                                                found = true;
                                                regOption();
                                                break;
                                    }
                        }
                        if (!found) {
                                    userRegistration.setUsername(username);
                                    System.out.println("Enter the password");
                                    String password = input.next();
                                    userRegistration.setPassword(password);
                                    display.println(userRegistration.getUsername() + ":" + userRegistration.getPassword());
                                    display.close();
                                    System.out.println("Regsitration is successfull");
                        }
                        homePage();
            }

           
            public static void login()  {
                        System.out.println("Enter the username");
                        String username = input.next();
                        boolean found = false;
                        try {
                                    input1 = new Scanner(new File("userLogin.txt"));
                        } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                        }
                        while (input1.hasNext() && !found) {
                                    String storedString = input1.next();
                                    String storedUsername[] = storedString.split(":");
                                    if (storedUsername[0].equals(username)) {
                                                System.out.println("Enter the password");
                                                String password = input.next();
                                                if (storedUsername[1].equals(password)) {
                                                            System.out.println("User Login is successful");
                                                            userlogin = storedUsername[0];
                                                            userDetails();
                                                            found = true;
                                                            break;
                                                }

                                    }
                        }
                        if (!found) {
                                    System.out.println("Invalid credential. Either username or password is incorrect");
                                    regOption();
                        }
            }

           
            public static void userDetails()  {
                        System.out.println("1. Enter social media details");
                        System.out.println("2. Retrive social media details");
           
                        String selection = input.next();
                        switch (selection) {
                        case "1":
                                    enterUserCredential();
                                    break;
                        case "2":
                                    retriveUserDetails();
                                    break;
                        default:
                                    System.out.println("Please select either option 1 or 2");
                                    userDetails();
                                    break;
                        }
            }

           
            public static void enterUserCredential() {
                        System.out.println("Enter the socialmedia name");
                        String sitename = input.next();
                        userCredential.setSitename(sitename);
                        if (validateDuplicateUser(sitename)) {
                                    System.out.println("Enter the " + sitename + " username");
                                    String username = input.next();
                                    userCredential.setSocialMediaUsername(username);
                                    System.out.println("Enter the " + sitename + " password");
                                    String password = input.next();
                                    userCredential.setSocialMediaPassword(password);
                                    display1.println(userlogin + ":" + userCredential.getSitename() + ":"
                                                            + userCredential.getSocialMediaUsername() + ":" + userCredential.getSocialMediaPassword());
                                    display1.close();
                        }
            }

        
            public static boolean validateDuplicateUser(String sitename) {
                        try {
                                    input2 = new Scanner(new File("userCredential.txt"));
                        } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                        }
                        while (input2.hasNext()) {
                                    String storedUserCredential = input2.next();
                                    String username[] = storedUserCredential.split(":");
                                    if (username[0].equals(userlogin) && username[1].equals(sitename)) {
                                                System.out.println("The site name already exists. Please try another site name");
                                                return false;
                                    }
                        }
                        return true;
            }

         
            public static void retriveUserDetails() {
                        System.out.println("Enter the site name to be retrived");
                        String sitename = input.next();
                        boolean found = false;
                        try {
                                    input2 = new Scanner(new File("userCredential.txt"));
                        } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                        }
                        while (input2.hasNext() && !found) {
                                    String storedUserCredential = input2.next();
                                    String username[] = storedUserCredential.split(":");
                                    if (username[0].equals(userlogin) && username[1].equals(sitename)) {
                                                String siteusername = username[2];
                                                String sitepassword = username[3];
                                                System.out.println("The " + sitename + " username is: " + siteusername);
                                                System.out.println("The " + sitename + " password is: " + sitepassword);
                                                found = true;
                                                break;
                                    }
                        }
                        if (!found) {
                                    System.out.println( sitename +" doesn't exist."+ "Please enter new sitename");
                        }
            }

        
        
}

